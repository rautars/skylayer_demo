-- This is simple demo for Skylayer mod which demonstrates how set default sky.

-- here your server default sky config (very minimal, see skylayer api for full configuration capabilities)
local applyDefaultSky = function(player_name)
    local sl = {}
    sl.name = "server_default_sky"
    sl.sun_data = {
        texture = "skylayer_demo_sun.png",
        scale = 3
    }
    sl.moon_data = {
        texture = "skylayer_demo_moon.png",
    }
    skylayer.add_layer(player_name, sl)
end

-- this will be invoked when player will join server
minetest.register_on_joinplayer(function(player)
    applyDefaultSky(player:get_player_name())
end)